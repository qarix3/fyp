<div class="col-lg-4 col-lg-offset-4">
    <h2>Change Profile</h2>
    <h5>Hello <span><?php echo $first_name; ?></span>.</h5>     
<?php 
    $fattr = array('class' => 'form-signin');
    echo form_open(site_url().'main/changeuser/', $fattr); ?>
    
    <div class="form-group">
      <?php echo form_input(array('name'=>'firstname', 'id'=> 'firstname', 'placeholder'=>'First Name', 'class'=>'form-control', 'value' => set_value('firstname', $groups->first_name))); ?>
      <?php echo form_error('firstname');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'lastname', 'id'=> 'lastname', 'placeholder'=>'Last Name', 'class'=>'form-control', 'value'=> set_value('lastname', $groups->last_name))); ?>
      <?php echo form_error('lastname');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'s_mcard', 'id'=> 's_mcard', 'placeholder'=>'Matric Card', 'class'=>'form-control', 'value'=> set_value('s_mcard'))); ?>
      <?php echo form_error('s_mcard');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'s_ic', 'id'=> 's_ic', 'placeholder'=>'IC', 'class'=>'form-control', 'value'=> set_value('s_ic'))); ?>
      <?php echo form_error('s_ic');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'s_type', 'id'=> 's_type', 'placeholder'=>'type', 'class'=>'form-control', 'value'=> set_value('s_type'))); ?>
      <?php echo form_error('s_type');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'s_plate', 'id'=> 's_plate', 'placeholder'=>'plate', 'class'=>'form-control', 'value'=> set_value('s_plate'))); ?>
      <?php echo form_error('s_plate');?>
    </div>
    <div class="form-group">
      <?php echo form_input(array('name'=>'email', 'id'=> 'email', 'placeholder'=>'Email', 'class'=>'form-control', 'value'=> set_value('email', $groups->email))); ?>
    </div>
    <div class="checkbox">
      <label>
        <input id="checkbox-pass" type="checkbox"> Change Password?
      </label>
    </div>
    <div id="hide-pass" style="display: none;">
      <div class="form-group">
        <?php echo form_password(array('name'=>'password', 'id'=> 'password', 'placeholder'=>'Password', 'class'=>'form-control', 'value' => set_value('password'))); ?>
        <?php echo form_error('password') ?>
      </div>
      <div class="form-group">
        <?php echo form_password(array('name'=>'passconf', 'id'=> 'passconf', 'placeholder'=>'Confirm Password', 'class'=>'form-control', 'value'=> set_value('passconf'))); ?>
        <?php echo form_error('passconf') ?>
      </div>
    </div>
    <?php echo form_submit(array('value'=>'Change', 'class'=>'btn btn-lg btn-primary btn-block')); ?>
    <?php echo form_close(); ?>
</div>